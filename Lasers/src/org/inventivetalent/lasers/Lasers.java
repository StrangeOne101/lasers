/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.lasers;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.inventivetalent.reflection.minecraft.Minecraft;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mcstats.MetricsLite;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;

public class Lasers extends JavaPlugin implements Listener {

	public static Lasers instance;
	public static String prefix = "§c[Lasers]§r ";

	public static File laserFile;

	public static    boolean      is1_8;
	protected static BannerHelper bannerHelper;

	public static long    LASER_INTERVAL      = 1;
	public static int     LASER_LENGTH        = 16;
	public static float   LASER_FREQUENCY     = 0.25f;
	public static boolean LASER_DAMAGE        = true;
	public static double  LASER_DAMAGE_AMOUNT = 0.25;
	public static boolean LASER_DAMAGE_FIRE   = true;
	public static boolean BLOCK_BLOCKS        = true;
	public static boolean BLOCK_ENTITY        = true;
	public static boolean COLOR_MIRROR        = true;
	public static boolean COLOR_GLASS_BLOCK   = true;
	public static boolean COLOR_GLASS_PANE    = true;
	public static boolean COLOR_MIX           = true;
	public static boolean DOUBLE_MIRRORS      = false;
	public static String  DEATH_MESSAGE       = "%player% tried to stand in front of a laser";
	public static int     RECEIVER_MODE       = 0;                                                // 0 = distance | 1 = color
	public static int     RECEIVER_TOLERANCE  = 25;
	public static int     ROTATOR_MODE        = 0;

	public Map<Location, Object[]> unloadedLasers = new HashMap<>();

	@Override
	public void onEnable() {
		instance = this;

		if (!Bukkit.getPluginManager().isPluginEnabled("ParticleLIB") && !classExists("de.inventivegames.particle.ParticleEffect")) {
			this.getLogger().severe("*****************************************");
			this.getLogger().severe("");
			this.getLogger().severe("   This plugin depends on ParticleLIB    ");
			this.getLogger().severe("         Please download it here         ");
			this.getLogger().severe(" http://www.spigotmc.org/resources/2067/ ");
			this.getLogger().severe("");
			this.getLogger().severe("*****************************************");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}

		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getPluginManager().registerEvents(new LasersListener(), this);

		is1_8 = Minecraft.VERSION.newerThan(Minecraft.Version.v1_8_R1);
		if (is1_8) {
			bannerHelper = new BannerHelper();
		}

		this.saveDefaultConfig();
		LASER_INTERVAL = this.getConfig().getLong("laser.interval", LASER_INTERVAL);
		LASER_LENGTH = this.getConfig().getInt("laser.length", LASER_LENGTH);
		LASER_FREQUENCY = (float) this.getConfig().getDouble("laser.frequency", LASER_FREQUENCY);
		LASER_DAMAGE = this.getConfig().getBoolean("laser.damage.enabled", LASER_DAMAGE);
		LASER_DAMAGE_AMOUNT = this.getConfig().getDouble("laser.damage.amount", LASER_DAMAGE_AMOUNT);
		LASER_DAMAGE_FIRE = this.getConfig().getBoolean("laser.damage.fire", LASER_DAMAGE_FIRE);
		COLOR_MIRROR = this.getConfig().getBoolean("color.mirror", COLOR_MIRROR);
		COLOR_GLASS_BLOCK = this.getConfig().getBoolean("color.glass.block", COLOR_GLASS_BLOCK);
		COLOR_GLASS_PANE = this.getConfig().getBoolean("color.glass.pane", COLOR_GLASS_PANE);
		COLOR_MIX = this.getConfig().getBoolean("color.mix", COLOR_MIX);
		BLOCK_BLOCKS = this.getConfig().getBoolean("blocked.blocks", BLOCK_BLOCKS);
		BLOCK_ENTITY = this.getConfig().getBoolean("blocked.entities", BLOCK_ENTITY);
		DOUBLE_MIRRORS = this.getConfig().getBoolean("mirrors.double", DOUBLE_MIRRORS);
		DEATH_MESSAGE = this.getConfig().getString("laser.damage.message", DEATH_MESSAGE);

		String mode = this.getConfig().getString("receiver.signal.mode", "");
		if ("distance".equals(mode.toLowerCase())) {
			RECEIVER_MODE = 0;
		}
		if ("color".equals(mode.toLowerCase())) {
			RECEIVER_MODE = 1;
		}
		RECEIVER_TOLERANCE = this.getConfig().getInt("receiver.signal.tolerance", 25);

		mode = this.getConfig().getString("mirrors.rotator.mode", "");
		if ("side".equals(mode.toLowerCase())) {
			ROTATOR_MODE = 0;
		}
		if ("strength".equalsIgnoreCase(mode.toLowerCase())) {
			ROTATOR_MODE = 1;
		}

		laserFile = new File(this.getDataFolder(), "lasers.json");
		if (!laserFile.exists()) {
			try {
				laserFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		Items.load();
		Recipes.load();

		Bukkit.getScheduler().runTaskLater(this, new Runnable() {
			@Override
			public void run() {
				getLogger().info("Loading lasers...");
				loadLasers();
				getLogger().info("Loaded " + LaserRunnable.lasers.size() + " laser blocks (" + unloadedLasers.size() + " will be loaded later)");
			}
		}, 40);

		try {
			MetricsLite metrics = new MetricsLite(this);
			if (metrics.start()) {
				this.getLogger().info("Metrics started");
			}
		} catch (Exception e) {
		}

		new LaserRunnable().runTaskTimer(Lasers.instance, LASER_INTERVAL, LASER_INTERVAL);
	}

	boolean classExists(String name) {
		try {
			Class<?> clazz = Class.forName(name);
			return clazz != null;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void onDisable() {
		if (laserFile != null) {
			this.saveLasers();
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (command.getName().equalsIgnoreCase("lasers")) {
			if (args.length == 0) {
				sender.sendMessage(prefix + "§b/lasers item <CRYSTAL|EMITTER|RECEIVER>");
				return true;
			}
			if ("item".equalsIgnoreCase(args[0])) {
				if (!sender.hasPermission("lasers.command.item")) {
					sender.sendMessage(prefix + "§cNo permission");
					return false;
				}
				if (!(sender instanceof Player)) {
					sender.sendMessage(prefix + "§cYou must be a player");
					return false;
				}
				if (args.length == 1) {
					sender.sendMessage(prefix + "§c/lasers item <CRYSTAL|EMITTER|RECEIVER>");
					return false;
				}
				if (args.length == 2) {
					if ("CRYSTAL".equalsIgnoreCase(args[1]) || "LASER_CRYSTAL".equalsIgnoreCase(args[1])) {
						((Player) sender).getInventory().addItem(Items.LASER_CRYSTAL);
					}
					if ("EMITTER".equalsIgnoreCase(args[1]) || "LASER_EMITTER".equalsIgnoreCase(args[1])) {
						((Player) sender).getInventory().addItem(Items.LASER_EMITTER);
					}
					if ("RECEIVER".equalsIgnoreCase(args[1]) || "LASER_RECEIVER".equalsIgnoreCase(args[1])) {
						((Player) sender).getInventory().addItem(Items.LASER_RECEIVER);
					}
					if ("ROTATOR".equalsIgnoreCase(args[1]) || "MIRROR_ROTATOR".equalsIgnoreCase(args[1])) {
						((Player) sender).getInventory().addItem(Items.MIRROR_ROTATOR);
					}
					return true;
				}

			}
		}
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> list = new ArrayList<>();
		if (args.length == 1) {
			list.add("item");
		}
		if (args.length > 1) {
			if ("item".equalsIgnoreCase(args[0])) {
				list.addAll(Arrays.asList("CRYSTAL", "EMITTER", "RECEIVER", "ROTATOR"));
			}
		}
		return TabCompletionHelper.getPossibleCompletionsForGivenArgs(args, list.toArray(new String[list.size()]));
	}

	void loadLasers() {
		try {
			BufferedReader in = new BufferedReader(new FileReader(laserFile));
			String json = "";
			String line = null;
			while ((line = in.readLine()) != null) {
				json += line;
			}
			in.close();
			if (json.isEmpty()) { return; }
			JSONArray array = new JSONArray(json);
			for (int i = 0; i < array.length(); i++) {
				JSONObject current = array.getJSONObject(i);
				JSONObject locObj = current.getJSONObject("location");
				Location location = new Location(Bukkit.getWorld(locObj.getString("world")), locObj.getInt("x"), locObj.getInt("y"), locObj.getInt("z"));
				boolean active = current.getBoolean("active");
				String type = current.getString("type");

				if (location.getWorld() == null) {
					continue;
				}

				if (location.getChunk().isLoaded()) {
					this.loadLaser(location, active, type);
				} else {
					this.unloadedLasers.put(location, new Object[] {
							Boolean.valueOf(active),
							type });
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void saveLasers() {
		JSONArray array = new JSONArray();
		for (Block b : LaserRunnable.lasers) {
			JSONObject current = new JSONObject();
			final Location loc = b.getLocation();

			ItemStack metaItem = null;
			for (MetadataValue meta : b.getMetadata("Lasers")) {
				if (meta.value() instanceof ItemStack) {
					metaItem = (ItemStack) meta.value();
					break;
				}
			}

			String type = null;
			if (Items.LASER_EMITTER.getType() == b.getType() && Items.LASER_EMITTER.isSimilar(metaItem)) {
				type = "EMITTER";
			}
			if (Items.LASER_RECEIVER.getType() == b.getType() && Items.LASER_RECEIVER.isSimilar(metaItem)) {
				type = "RECEIVER";
			}
			if (Items.MIRROR_ROTATOR.getType() == b.getType() && Items.MIRROR_ROTATOR.isSimilar(metaItem)) {
				type = "ROTATOR";
			}
			if (type == null) {
				continue;
			}
			current.put("type", type);
			current.put("location", new JSONObject() {
				{
					this.put("world", loc.getWorld().getName());
					this.put("x", loc.getBlockX());
					this.put("y", loc.getBlockY());
					this.put("z", loc.getBlockZ());
				}
			});
			current.put("active", LaserRunnable.activeLasers.contains(b));

			array.put(current);
		}
		for (Entry<Location, Object[]> entry : this.unloadedLasers.entrySet()) {
			final Location loc = entry.getKey();
			final Boolean bool = (Boolean) entry.getValue()[0];
			String type = (String) entry.getValue()[1];

			JSONObject current = new JSONObject();
			if (type == null) {
				continue;
			}
			current.put("type", type);
			current.put("location", new JSONObject() {
				{
					this.put("world", loc.getWorld());
					this.put("x", loc.getBlockX());
					this.put("y", loc.getBlockY());
					this.put("z", loc.getBlockZ());
				}
			});
			current.put("active", bool);

			array.put(current);
		}

		String jsonString = array.toString(2);
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(laserFile));
			out.write(jsonString);
			out.flush();
			out.close();
		} catch (IOException e) {
			this.getLogger().log(Level.SEVERE, "Exception while saving Lasers to file", e);
		}
	}

	boolean loadLaser(Location loc, boolean active, String type) {
		if (loc.getChunk().isLoaded()) {
			loc.getBlock().setMetadata("Lasers", new FixedMetadataValue(Lasers.instance, "EMITTER".equals(type) ? Items.LASER_EMITTER : "RECEIVER".equals(type) ? Items.LASER_RECEIVER : "ROTATOR".equals(type) ? Items.MIRROR_ROTATOR : null));
			LaserRunnable.lasers.add(loc.getBlock());
			if (active && "EMITTER".equals(type)) {
				LaserRunnable.activeLasers.add(loc.getBlock());
			}
			return true;
		}
		return false;
	}

	@EventHandler
	public void onChunkLoad(ChunkLoadEvent e) {
		Iterator<Entry<Location, Object[]>> iterator = this.unloadedLasers.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Location, Object[]> entry = iterator.next();
			Location loc = entry.getKey();
			boolean active = (Boolean) entry.getValue()[0];
			String type = (String) entry.getValue()[1];
			if (loc.getChunk().isLoaded()) {
				if (this.loadLaser(loc, active, type)) {
					iterator.remove();
				}
			}
		}
	}

}
