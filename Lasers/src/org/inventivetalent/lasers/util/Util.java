/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.lasers.util;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.inventivetalent.reflection.resolver.minecraft.NMSClassResolver;
import org.inventivetalent.reflection.resolver.minecraft.OBCClassResolver;

public class Util {

	static NMSClassResolver nmsClassResolver = new NMSClassResolver();
	static OBCClassResolver obcClassResolver = new OBCClassResolver();

	static Class<?> obcCraftInventory       = obcClassResolver.resolveSilent("inventory.CraftInventory");
	static Class<?> obcCraftInventoryCustom = obcClassResolver.resolveSilent("inventory.CraftInventoryCustom");
	static Class<?> obcMinecraftInventory   = obcClassResolver.resolveSilent("inventory.CraftInventoryCustom$MinecraftInventory");
	static Class<?> tileEntityHopper        = nmsClassResolver.resolveSilent("TileEnttiyHopper");

	public static Object getEntityBoundingBox(Entity ent) {
		try {
			Object handle = Reflection.getHandle(ent);
			Object boundingBox = Reflection.getNMSClass("Entity").getDeclaredMethod("getBoundingBox").invoke(handle);
			return boundingBox;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean entityBoundingBoxContains(Object boundingBox, Vector point) {
		try {
			double minX = boundingBox.getClass().getDeclaredField("a").getDouble(boundingBox);
			double minY = boundingBox.getClass().getDeclaredField("b").getDouble(boundingBox);
			double minZ = boundingBox.getClass().getDeclaredField("c").getDouble(boundingBox);
			double maxX = boundingBox.getClass().getDeclaredField("d").getDouble(boundingBox);
			double maxY = boundingBox.getClass().getDeclaredField("e").getDouble(boundingBox);
			double maxZ = boundingBox.getClass().getDeclaredField("f").getDouble(boundingBox);

			if (point.getX() >= minX && point.getX() <= maxX) {
				if (point.getZ() >= minZ && point.getZ() <= maxZ) {
					if (point.getY() >= minY && point.getY() <= maxY) { return true; }
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private static Class<?> nmsChatSerializer    = nmsClassResolver.resolveSilent("ChatSerializer", "IChatBaseComponent$ChatSerializer");
	private static Class<?> nmsPacketPlayOutChat = nmsClassResolver.resolveSilent("PacketPlayOutChat");

	public static void sendRawMessage(Player player, String message) {
		sendRawMessage(player, message, 0);
	}

	public static void sendRawMessage(Player player, String message, int position) {
		sendRawMessage(player, message, position, true);
	}

	public static String StringToTellraw(String string) {
		String raw = "{\"text\":\"" + string + "\"}";
		return raw;
	}

	/**
	 * Send a tellraw Message to a Player
	 *
	 * @param player   Player
	 * @param message  Message
	 * @param position Position of the Message
	 * @param flag     System message? (Default=<code>true</code>)
	 */
	public static void sendRawMessage(Player player, String message, int position, boolean flag) {
		try {
			Object handle = Reflection.getHandle(player);
			Object connection = Reflection.getField(handle.getClass(), "playerConnection").get(handle);
			Object serialized = Reflection.getMethod(nmsChatSerializer, "a", new Class[] { String.class }).invoke(null, new Object[] { message });
			Object packet = null;
			if (Reflection.getVersion().contains("1_7")) {
				packet = nmsPacketPlayOutChat.getConstructor(new Class[] {
						Reflection.getNMSClass("IChatBaseComponent"),
						int.class,
						boolean.class }).newInstance(new Object[] {
						serialized,
						position,
						flag });
			} else if (Reflection.getVersion().contains("1_8")) {
				packet = nmsPacketPlayOutChat.getConstructor(new Class[] {
						Reflection.getNMSClass("IChatBaseComponent"),
						byte.class }).newInstance(new Object[] {
						serialized,
						(byte) position });
			}
			if (packet != null) {
				Reflection.getMethod(connection.getClass(), "sendPacket", new Class[0]).invoke(connection, new Object[] { packet });
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
