/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.lasers;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public abstract class Items {

	public static ItemStack	LASER_CRYSTAL;
	public static ItemStack	LASER_EMITTER;
	public static ItemStack	LASER_RECEIVER;
	public static ItemStack	MIRROR_ROTATOR;

	private static ItemMeta	meta;

	protected static void load() {
		/* Laser Crystal */
		LASER_CRYSTAL = new ItemStack(Material.DIAMOND);
		meta = LASER_CRYSTAL.getItemMeta();
		meta.setDisplayName(ChatColor.RED + "Laser " + ChatColor.AQUA + "Crystal");
		meta.addEnchant(Enchantment.DAMAGE_ALL, 1, true);
		LASER_CRYSTAL.setItemMeta(meta);

		/* Laser Emitter */
		LASER_EMITTER = new ItemStack(Material.DISPENSER);
		meta = LASER_EMITTER.getItemMeta();
		meta.setDisplayName(ChatColor.RED + "Laser Emitter");
		LASER_EMITTER.setItemMeta(meta);

		/* Laser Receiver */
		LASER_RECEIVER = new ItemStack(Material.DROPPER);
		meta = LASER_RECEIVER.getItemMeta();
		meta.setDisplayName(ChatColor.RED + "Laser Receiver");
		LASER_RECEIVER.setItemMeta(meta);

		/* Mirror Rotator */
		MIRROR_ROTATOR = new ItemStack(Material.DISPENSER);
		meta = MIRROR_ROTATOR.getItemMeta();
		meta.setDisplayName(ChatColor.AQUA + "Mirror Rotator");
		MIRROR_ROTATOR.setItemMeta(meta);
	}

}
